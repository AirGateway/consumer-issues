# AirGateway Issue Tracker

Welcome to [AirGateway](https://airgateway.com/) Issue Tracker.

In this repository you can create, check and track any issue you may encounter using our services and APIs.

- [See open issues](https://gitlab.com/AirGateway/consumer-issues/issues) (public issues and your own private ones)
- [Open a new one](https://gitlab.com/AirGateway/consumer-issues/issues/new)

Please, remember to join our [Gitter](https://gitter.im/AirGateway/Lobby) to perform more direct communication and receive latest updates.

Regarding the status of our systems, you can keep track of it in:

- [Status page](https://status.airgateway.net/)
- [Status Twitter account](https://twitter.com/airgtwystatus/)